const Sequelize = require("sequelize");

const sequelize = require("../util/database");

const User = sequelize.define("user", {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true,
  },
  firstName: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notNull: { msg: "first name is required" },
    },
  },
  lastName: Sequelize.STRING,
  email: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notNull: { msg: "email is required" },
    },
  },
  phone: {
    type: Sequelize.INTEGER,
    allowNull: false,
    validate: {
      notNull: { msg: "Phone No is required" },
    },
  },
});

module.exports = User;
